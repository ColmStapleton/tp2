﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2_Classe
{
    class Client
    {
        private String cin;
        private String nom;
        private String prenom;
        private String tel;

        public String CIN { get => cin; private set => cin = value; }
        public String Nom { get => nom; set => nom = value; }
        public String Prenom { get => prenom; set => prenom = value; }
        public String Tel { get => tel; set => tel = value; }

        public Client(String cin, String nom, String prenom)
        {
            this.CIN = cin;
            this.Nom = nom;
            this.Prenom = prenom;
        }

        public Client(String cin, String nom, String prenom, String tel)
        {
            this.CIN = cin;
            this.Nom = nom;
            this.Prenom = prenom;
            this.Tel = tel;
        }

        public void Afficher()
        {
            Console.WriteLine("CIN : {0}\nNom : {1}\nPrénom : {2}\nNuméro de téléphone : {3}", this.CIN, this.Nom, this.Prenom, this.Tel);
        }
    }   
}
