﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2_Classe
{
    class Compte
    {
        private static int compteur = 0;

        private double solde = 0;
        private int codeCompte;
        private Client owner;

        public double Solde { get => solde;}
        public int Code { get => codeCompte; }
        public Client Owner { get => owner; set => owner = value; }

        public Compte(Client owner)
        {
            this.Owner = owner;
            this.codeCompte = compteur++;
        }

        public void Crediter(int montant)
        {
            solde += montant;
            Console.WriteLine("Opération bien effectuée.");
        }

        public void Crediter (int montant, Compte compteADebiter)
        {
            solde += montant;
            compteADebiter.Debiter(montant);
        }

        public void Debiter (int montant)
        {
            if (solde-montant < 0)
            {
                throw new Exception("Opération impossible. Le compte {0} de {1} {2} n'a pas suffisamment de ressources.");
            }
            else
            {
                solde -= montant;
                Console.WriteLine("Opération bien effectuée.");
            }
        }

        public void Debiter (int montant, Compte compteACrediter)
        {
            if (solde - montant < 0)
            {
                throw new Exception("Opération impossible. Le compte {0} de {1} {2} n'a pas suffisamment de ressources.");
            }
            else
            {
                solde -= montant;
                compteACrediter.Crediter(montant);
            }          
        }

        public void Afficher ()
        {
            Console.WriteLine("Détails du Compte\n**************\nNuméro du compte : {0}\nSolde du compte : {1}\nPropriétaire du compte",this.Code,this.Solde);
            Owner.Afficher();
            Console.WriteLine();
        }

        public static void AfficherNombreComptes()
        {
            Console.WriteLine("Nombre de comptes crées : {0}", compteur);
        }
    }
}
