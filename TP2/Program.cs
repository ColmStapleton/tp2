﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2_Classe
{
    class Program
    {
        static void Main(string[] args)
        {

            Client client1 = new Client("ABC123", "Stapleton", "Colm", "0650563579");
            Compte compte1 = new Compte(client1);
            compte1.Afficher();

            Compte.AfficherNombreComptes();

            Client client2 = new Client("DEF456", "Schmitt", "Pierre", "0123456789");
            Compte compte2 = new Compte(client2);
            compte2.Afficher();

            Compte.AfficherNombreComptes();

            Client client3 = new Client("HIJ456", "Dupont", "René", "0123456789");
            Compte compte3 = new Compte(client3);
            Compte.AfficherNombreComptes();

            compte1.Crediter(5000);
            compte1.Afficher();
            compte2.Crediter(3000, compte1);
            compte2.Afficher();
            compte1.Afficher();


            compte2.Debiter(1000, compte3);
            compte1.Afficher();
            compte2.Afficher();
            compte3.Afficher();

            compte3.Debiter(1500, compte1);
            compte3.Afficher();
            Console.ReadLine();
        }
    }
}
