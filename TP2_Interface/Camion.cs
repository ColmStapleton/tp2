﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2_Interface
{
    public class Camion : IVehicules
    {
        private int code;
        private int puissanceMoteur;
        private String couleur;
        private int capaciteRemorque;
        private string constructeur;

        public int Code { get => this.code; set => this.code = value; }
        public int PuissanceMoteur { get => this.puissanceMoteur; set => this.puissanceMoteur = value; }
        public string Couleur { get => this.couleur; set => this.couleur = value; }
        public int CapaciteRemorque { get => this.capaciteRemorque; set => this.capaciteRemorque = value; }
        public string Constructeur { get => this.constructeur; set => this.constructeur = value; }

        public Camion()
        {
               
        }

        public void Saisie()
        {
            Console.WriteLine("Veuillez saisir le code du camion :");
            this.Code = int.Parse(Console.ReadLine());

            Console.WriteLine("Veuillez saisir la puissance moteur :");
            Boolean valid = false;
            while (!valid)
            {
                try
                {
                    this.PuissanceMoteur = int.Parse(Console.ReadLine());
                    valid = true;
                }
                catch
                {
                    Console.WriteLine("Veuillez saisir un entier pour la puissance moteur.");
                }               
            }
          
            Console.WriteLine("Veuillez saisir la couleur du camion :");
            this.Couleur = Console.ReadLine();

            Console.WriteLine("Veuillez saisir la capacité de la remorque en kg :");
            valid = false;
            while (!valid)
            {
                try
                {
                    this.CapaciteRemorque = int.Parse(Console.ReadLine());
                    valid = true;
                }
                catch
                {
                    Console.WriteLine("Veuillez saisir un entier pour la capacité de la remorque.");
                }
            }

            Console.WriteLine("Veuillez saisir le constructeur du camion :");
            this.Constructeur = Console.ReadLine();

        }

        public override string ToString()
        {
            return String.Format("Code : {0}\nPuissance : {1}\nCouleur : {2}\nCapacité de la remorque (kg): {3}\nConstructeur : {4}\n", this.Code, this.PuissanceMoteur, this.Couleur, this.CapaciteRemorque, this.Constructeur);
        }
    }
}
