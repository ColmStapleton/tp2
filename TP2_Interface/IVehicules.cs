﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2_Interface
{
    interface IVehicules
    {
        int Code { get; set; }
        int PuissanceMoteur { get; set; }
        String Couleur { get; set; }
        String Constructeur { get; set; }

        void Saisie();
        String ToString();

    }
}
