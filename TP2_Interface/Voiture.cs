﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2_Interface
{
    public class Voiture : IVehicules
    {
        private int code;
        private int puissanceMoteur;
        private String couleur;
        private int nbPlaces;
        private string constructeur;

        public int Code { get => this.code; set => this.code = value; }
        public int PuissanceMoteur { get => this.puissanceMoteur; set => this.puissanceMoteur = value; }
        public string Couleur { get => this.couleur; set => this.couleur = value; }
        public int NbPlaces { get => this.nbPlaces; set => this.nbPlaces = value; }
        public string Constructeur { get => this.constructeur; set => this.constructeur = value; }

        public void Saisie()
        {
            Console.WriteLine("Veuillez saisir le code de la voiture :");
            this.Code = int.Parse(Console.ReadLine());

            Console.WriteLine("Veuillez saisir la puissance moteur :");
            Boolean valid = false;
            while (!valid)
            {
                try
                {
                    this.PuissanceMoteur = int.Parse(Console.ReadLine());
                    valid = true;
                }
                catch
                {
                    Console.WriteLine("Veuillez saisir un entier pour la puissance moteur.");
                }
            }

            Console.WriteLine("Veuillez saisir la couleur de la voiture :");
            this.Couleur = Console.ReadLine();

            Console.WriteLine("Veuillez saisir le nombre de places de la voiture :");
            valid = false;
            while (!valid)
            {
                try
                {
                    this.NbPlaces = int.Parse(Console.ReadLine());
                    valid = true;
                }
                catch
                {
                    Console.WriteLine("Veuillez saisir un entier pour le nombre de places.");
                }
            }

            Console.WriteLine("Veuillez saisir le constructeur de la voiture :");
            this.Constructeur = Console.ReadLine();

        }

        public override string ToString()
        {
            return String.Format("Code : {0}\nPuissance : {1}\nCouleur : {2}\nNombre de places assises : {3}\nConstructeur : {4}\n", this.Code, this.PuissanceMoteur, this.Couleur, this.NbPlaces, this.Constructeur);
        }
    }
}
