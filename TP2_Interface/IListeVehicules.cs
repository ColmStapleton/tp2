﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2_Interface
{
    interface IListeVehicules
    {
        double Moyenne { get; set; }
        int Max { get; set; }
        void Affichage();
        void Ajout();
        void Recherche();

    }
}
