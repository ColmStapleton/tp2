﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2_Polymorphisme
{
    class Program
    {
        static void Main(string[] args)
        {


            Employe e1 = new Employe("Bibi","Toto",new DateTime(1998,10,10),1500);
            Employe e2 = new Employe("Baba", "Titi", new DateTime(1997, 10, 10), 1550);
            Employe e3 = new Employe("Bubu", "Tete", new DateTime(1996, 10, 10), 1450);
            Employe e4 = new Employe("Bebe", "Tutu", new DateTime(1995, 10, 10), 1300);
            Employe e5 = new Employe("Bobo", "Tata", new DateTime(1994, 10, 10), 1350);

            Chef c1 = new Chef("Jojo","Mama",new DateTime(1988,8,5),2500,"Comptabilité");
            Chef c2 = new Chef("Polo", "Riri", new DateTime(1986, 2, 5), 2700, "Achats");

            Directeur d1 = new Directeur("Stapleton","Colm",new DateTime(1988,10,17),5000,"IT","MaSociété");

            Personne[] tab = new Personne[] { e1, e2, e3, e4, e5, c1, c2, d1 };

            for (int i = 0; i < tab.Length; i++)
            {
                Console.WriteLine(tab[i].Afficher());
                Console.WriteLine();
            }

            foreach (Personne pers in tab)
            {
                Console.WriteLine(pers.Afficher());
                Console.WriteLine();
            }

            Console.ReadLine();
        }
    }
}
