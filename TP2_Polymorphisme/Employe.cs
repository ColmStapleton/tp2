﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2_Polymorphisme
{
    public class Employe : Personne
    {
        private int salaire;
        public Employe(string nom, string prenom, DateTime dob, int salaire) : base(nom, prenom, dob)
        {
            this.salaire = salaire;
        }

        public override String Afficher()
        {
            return String.Format("{0}\nSalaire : {1}",base.Afficher(),this.salaire);
        }
    }
}
