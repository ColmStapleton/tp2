﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2_Polymorphisme
{
    public class Personne
    {
        private String nom;
        private String prenom;
        private DateTime dob;

        public Personne(String nom, String prenom, DateTime dob)
        {
            this.nom = nom;
            this.prenom = prenom;
            this.dob = dob;
        }

        public virtual String Afficher()
        {
            return String.Format("Nom : {0}\nPrénom : {1}\nDate de naissance : {2}", this.nom, this.prenom, this.dob);
        }
    }
}
