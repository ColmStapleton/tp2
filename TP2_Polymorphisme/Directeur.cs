﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2_Polymorphisme
{
    public class Directeur : Chef
    {
        private String societe;
        public Directeur(string nom, string prenom, DateTime dob, int salaire, string service, string societe) : base(nom, prenom, dob, salaire, service)
        {
            this.societe = societe;
        }

        public override string Afficher()
        {
            return String.Format("{0}\nSociété : {1}", base.Afficher(), this.societe);
        }
    }
}
