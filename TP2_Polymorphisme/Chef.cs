﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2_Polymorphisme
{
    public class Chef : Employe
    {
        private String service;
        public Chef(string nom, string prenom, DateTime dob, int salaire, string service) : base(nom, prenom, dob, salaire)
        {
            this.service = service;
        }

        public override String Afficher()
        {
            return String.Format("{0}\nService : {1}", base.Afficher(), this.service);
        }
    }
}
